package com.psybergate.grad2020.oo_part3.hw9a;

/**
 * 
 * @since 24 Feb 2020
 */
public class Client {

  public Client() {
  }

  public static void main(String[] args) {
    Employee e1 = new Employee("A001", "Administrator", "John", "Smith", 120_000.0);
    Employee e2 = new Employee("A002", "Administrator", "Jane", "Doe", 80_000.0);
    Employee e3 = new Employee("A003", "Administrator", "Gary", "Sinise", 250_000.0);
    Employee e4 = new Employee("A010", "Manager", "Craig", "Bart", 300_000.0);
    Employee e5 = new Employee("A011", "Manager", "Shirley", "Norman", 520_000.0);
    Employee e6 = new Employee("A020", "Director", "Vincent", "Radebe", 600_000.0);
    Employee e7 = new Employee("A021", "Director", "Sipho", "Msimanga", 1_200_000.0);

    printObjects(e1, e2, e3, e4, e5, e6, e7);
  }

  private static void printObjects(final Employee... employees) {
    for (Employee employee : employees) {
      System.out.println(employee);
    }
  }
}
