package com.psybergate.grad2020.oo_part3.hw9a;

/**
 * 
 * @since 24 Feb 2020
 */
public class Employee {

  private final String employeeNum;

  private EMPLOYEE_TYPE employeeType;

  private String name;

  private String surname;

  private double annualSalary;

  private static enum EMPLOYEE_TYPE {
    ADMINISTRATOR, DIRECTOR, MANAGER, NULL
  }

  public Employee(final String employeeNum, final String employeeType, final String name,
      final String surname, final double annualSalary) {
    this.employeeNum = employeeNum;
    this.name = name;
    this.surname = surname;
    this.annualSalary = annualSalary;
    this.setEmployeeType(employeeType);
  }

  private final void setEmployeeType(final String employeeType) {
    switch (employeeType.toUpperCase()) {
      case "ADMINISTRATOR":
        this.employeeType = EMPLOYEE_TYPE.ADMINISTRATOR;
        break;
      case "DIRECTOR":
        this.employeeType = EMPLOYEE_TYPE.DIRECTOR;
        break;
      case "MANAGER":
        this.employeeType = EMPLOYEE_TYPE.MANAGER;
        break;
      default:
        this.employeeType = EMPLOYEE_TYPE.NULL;
        break;
    }
  }

  private final double toPercentage(final double perc) {
    return perc / 100;
  }

  private final double getContribution(final double perc, final double amount) {
    return toPercentage(perc) * amount;
  }

  public final double getMedicalAidContribution() {
    final double monthlySalary = annualSalary / 12;
    switch (this.employeeType) {
      case DIRECTOR:
        return 5000.0;
      case MANAGER:
        if (getContribution(7.5, monthlySalary) > 1200.0) {
          return (getContribution(7.5, monthlySalary) > 3000.0) ? 3000.0
              : getContribution(7.5, monthlySalary);
        }
        else
          return 1200.0;
      case ADMINISTRATOR:
        if (getContribution(10, monthlySalary) > 800.0) {
          return (getContribution(10, monthlySalary) > 2000.0) ? 2000.0
              : getContribution(10, monthlySalary);
        }
        else
          return 800.0;
      default:
        return 0;
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "\n{EmployeeNum := " + employeeNum + ", EmployeeType := "
        + employeeType + ", EmployeeName := " + name + ", EmployeeSurname := " + surname
        + ", AnnualSalary := R" + String.format("%,.2f", annualSalary)
        + ", MedicalAidContribution := R" + String.format("%,.2f", getMedicalAidContribution())
        + "}";
  }

}
